<?php

namespace Martinsl\DesafioUm\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @param string $fieldId
     * @param string $groupId
     * @return mixed
     */
    public function getConfig($fieldId, $groupId = 'general')
    {
        return $this->scopeConfig->getValue("desafio_um/{$groupId}/{$fieldId}");
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return (string)$this->getConfig('api_key');
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return (string)$this->getConfig('endpoint');
    }

    /**
     * @return bool
     */
    public function getIsEnabled()
    {
        return (bool)$this->getConfig('is_enabled');
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger()
    {
        return $this->_logger;
    }
}
