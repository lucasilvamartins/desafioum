<?php

namespace Martinsl\DesafioUm\Model\Erp;

class Order
{
    /** @var \Martinsl\DesafioUm\Helper\Data $dataHelper */
    protected $dataHelper;

    /** @var \Magento\Framework\Serialize\Serializer\Json */
    protected $jsonSerializer;

    /** @var \Magento\Framework\HTTP\ZendClientFactory */
    protected $httpClientFactory;

    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    public function __construct(
        \Martinsl\DesafioUm\Helper\Data $dataHelper,
        \Magento\Framework\Serialize\Serializer\Json $jsonSerializer,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
    ) {
        $this->dataHelper = $dataHelper;
        $this->jsonSerializer = $jsonSerializer;
        $this->httpClientFactory = $httpClientFactory;

        $this->logger = $dataHelper->getLogger();
    }

    public function processNewOrder($order)
    {
        $apiKey = $this->dataHelper->getApiKey();
        $endpoint = $this->dataHelper->getEndpoint();

        $generalData = $this->formatData($order);

        $client = $this->httpClientFactory->create();
        $client->setUri($endpoint);
        $client->setMethod(\Zend_Http_Client::POST);
        $client->setHeaders(\Zend_Http_Client::CONTENT_TYPE, 'application/json');
        $client->setHeaders('Accept', 'application/json');
        $client->setHeaders('Authorization', 'Bearer ' . $apiKey);
        $client->setRawData($generalData, 'application/json');
        $response = $client->request();

        if (\Magento\Framework\Webapi\Response::HTTP_OK === $response->getStatus()) {
            $this->logger->info("Successfully sent order {$order->getIncrementId()} to ERP", [$response->getBody()]);
        } else {
            $this->logger->info("Failed to send order {$order->getIncrementId()} to ERP", [$response->getBody()]);
        }
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return bool|false|string
     */
    protected function formatData($order)
    {
        return $this->jsonSerializer->serialize([
            'increment_id' => $order->getIncrementId(),
            'shipping' => $this->formatAddressData($order),
            'customer' => $this->formatCustomerData($order),
            'items' => $this->formatItemsInfo($order->getAllVisibleItems()),
            'subtotal' => $order->getSubtotal(),
            'shipping_amount' => $order->getShippingAmount(),
            'discount' => $order->getDiscount(),
            'total' => $order->getGrandTotal()
        ]);
    }

    /**
     * @param $itemsCollection
     * @return array
     */
    protected function formatItemsInfo($itemsCollection)
    {
        $data = [];
        foreach ($itemsCollection as $item) {
            if (null !== $item->getParentItem()) {
                continue;
            }
            $data[] = [
                'sku' => $item->getSku(),
                'name' => $item->getName(),
                'price' => $item->getPrice(),
                'qty' => $item->getQtyOrdered()
            ];
        }
        return $data;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return array
     */
    protected function formatAddressData($order)
    {
        $shippingAddress = $order->getShippingAddress();
        return [
            'street' => $shippingAddress->getStreetLine(1),
            'number' => $shippingAddress->getStreetLine(2),
            'neighborhood' => $shippingAddress->getStreetLine(3),
            'additional' => $shippingAddress->getStreetLine(4),
            'city' => $shippingAddress->getCity(),
            'uf' => $shippingAddress->getRegion(),
            'country' => $shippingAddress->getCountryId()
        ];
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return array
     */
    protected function formatCustomerData($order)
    {
        $shippingAddress = $order->getShippingAddress();

        return [
            'name' => $order->getCustomerName(),
            'cpf' => $order->getCustomerTaxVat(),
            'telephone' => $shippingAddress->getTelephone()
        ];
    }
}
