<?php

namespace Martinsl\DesafioUm\Observer;

class Listener implements \Magento\Framework\Event\ObserverInterface
{
    /** @var \Martinsl\DesafioUm\Helper\Data $dataHelper */
    protected $dataHelper;

    /** @var \Psr\Log\LoggerInterface */
    protected $logger;

    /** @var \Martinsl\DesafioUm\Model\Erp\Order */
    protected $erpOrder;

    public function __construct(
        \Martinsl\DesafioUm\Helper\Data $dataHelper,
        \Martinsl\DesafioUm\Model\Erp\Order $erpOrder
    ) {
        $this->dataHelper = $dataHelper;
        $this->erpOrder = $erpOrder;

        $this->logger = $dataHelper->getLogger();
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this|void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->dataHelper->getIsEnabled()) {
            return $this;
        }

        $order = $observer->getOrder();

        if (false === $order->isObjectNew()) {
            return $this;
        }

        $this->logger->debug("BEGIN - ERP order {$order->getIncrementId()} processing");

        try {
            $this->erpOrder->processNewOrder($order);
        } catch (\Exception $e) {
            $this->logger->info("Failed to send order {$order->getIncrementId()} to ERP ", [$e->getMessage()]);
        }
        $this->logger->debug("END - ERP order {$order->getIncrementId()} processing");

        return $this;
    }
}
